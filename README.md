# SAST Failure Example

A reduced test case for [gitlab-org/gitlab#300368](https://gitlab.com/gitlab-org/gitlab/-/issues/300368).

## Created via

- `dotnet new console`
- `dotnet new sln`
- `dotnet sln add ./sast-failure-example.csproj`
- Added a bunch of dependencies from projects which exhibit this failure.
